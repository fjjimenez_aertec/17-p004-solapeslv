﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Data;
using Aertec.SolapesLV.Entities;
using Aertec.SolapesLV.Common;

namespace Aertec.SolapesLV.ViewModels
{
    public class PuntosControlViewModel:ViewModelBase
    {
        private ICollectionView _puntosControl;

        public ICollectionView PuntosControl//collection used in FilterView.xaml
        {
            get { return _puntosControl; }
        }

        public PuntosControlViewModel()
        {
            _puntosControl = CollectionViewSource.GetDefaultView(createListaSolapes());
            _puntosControl.Filter = GroupFilters;
        }

        private bool GroupFilters(object item)
        {
            var solapeEntity = item as SolapesEntity;
            return solapeEntity != null &&
                (
                     solapeEntity.Prio.Contains(PrioSearchString) &&
                     solapeEntity.RESP.Contains(RespSearchString) &&
                     solapeEntity.LANZAR.Contains(LanzarSearchString) &&
                     solapeEntity.DESCRIPCION.Contains(DescripcionSearchString) &&
                     solapeEntity.WOOP.Contains(WoopSearchString) &&
                     solapeEntity.PERFIL.Contains(PerfilSearchString) &&
                     solapeEntity.ACCION.Contains(AccionSearchString) &&
                     solapeEntity.COMENTARIO.Contains(ComentarioSearchString)
                     &&
                     (
                     ((EsNoche && solapeEntity.TURNO.Equals(EnumTurnos.Noche)) ||
                      (EsTarde && solapeEntity.TURNO.Equals(EnumTurnos.Tarde)) ||
                      (EsMañana && solapeEntity.TURNO.Equals(EnumTurnos.Mañana)))
                     ||
                     (EsTodos && (solapeEntity.TURNO.Equals(EnumTurnos.Noche) || solapeEntity.TURNO.Equals(EnumTurnos.Tarde) || solapeEntity.TURNO.Equals(EnumTurnos.Mañana)))
                     )
                 );
               
        }

        private List<SolapesEntity> createListaSolapes()
        {
            List<SolapesEntity>  lista = new List<SolapesEntity>();

            for (int i = 0; i < 40; i++)
            {
                lista.Add(new SolapesEntity()
                {
                    ACCION = "accion" + i,
                    COMENTARIO = "comentario" + i + "\r\nasdsdsdsad\r\nasdasddsa",
                    CRITICO = false,
                    TURNO=EnumTurnos.Tarde,
                    DESCRIPCION = "descripcion" + i,
                    ETLB = "eTLB" + i,
                    HECHO = true,
                    LANZAR = "lanzar" + i,
                    PERFIL = "perfil" + i,
                    Prio = "prio" + i,
                    RESP = "resp" + i,
                    WOOP = "wo-op" + i,
                    WHEN = DateTime.Today.AddDays(i)
                });
            }
            //datagrid.DataContext = this;
            return lista;
        }


        private String _prioSearchString="";
        public String PrioSearchString
        {
            get { return _prioSearchString; }
            set
            {
                _prioSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _respSearchString = "";
        public String RespSearchString
        {
            get { return _respSearchString; }
            set
            {
                _respSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _lanzarSearchString = "";
        public String LanzarSearchString
        {
            get { return _lanzarSearchString; }
            set
            {
                _lanzarSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _eTLBSearchString = "";
        public String ETLBSearchString
        {
            get { return _eTLBSearchString; }
            set
            {
                _eTLBSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _descripcionSearchString = "";
        public String DescripcionSearchString
        {
            get { return _descripcionSearchString; }
            set
            {
                _descripcionSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _woopSearchString = "";
        public String WoopSearchString
        {
            get { return _woopSearchString; }
            set
            {
                _woopSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _perfilSearchString = "";
        public String PerfilSearchString
        {
            get { return _perfilSearchString; }
            set
            {
                _perfilSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _accionSearchString = "";
        public String AccionSearchString
        {
            get { return _accionSearchString; }
            set
            {
                _accionSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private String _comentarioSearchString = "";
        public String ComentarioSearchString
        {
            get { return _comentarioSearchString; }
            set
            {
                _comentarioSearchString = value;
                _puntosControl.Refresh();
            }
        }

        private bool _esNoche;
        public bool EsNoche
        {
            get { return _esNoche; }
            set
            {
                _esNoche = value;
                _puntosControl.Refresh();
            }

        }

        private bool _esTarde;
        public bool EsTarde
        {
            get { return _esTarde; }
            set
            {
                _esTarde = value;
                _puntosControl.Refresh();
            }

        }

        private bool _esMañana;
        public bool EsMañana
        {
            get { return _esMañana; }
            set
            {
                _esMañana = value;
                _puntosControl.Refresh();
            }

        }


        public bool EsTodos
        {
            get { return !EsTarde && !EsMañana && !EsNoche; }
        }
    }
}
