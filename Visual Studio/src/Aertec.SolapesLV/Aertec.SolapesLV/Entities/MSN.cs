﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Configuration;
using System.Windows.Media;

namespace Aertec.SolapesLV.Entities
{
    public class MSN
    {
        public string Number { get; set; }
        public string Country { get; set; }
        public ImageSource Image
        {
            get
            {
                
               // return ImageSource
                return new BitmapImage(new Uri("pack://application:,,,/Resources/" + Country + ".png"));
                
                
            }
        }
    }
}
