﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aertec.SolapesLV.Common;

namespace Aertec.SolapesLV.Entities
{
    public class SolapesEntity
    {
        public string Prio { get; set; }
        public string RESP { get; set; }
        public string LANZAR { get; set; }
        public EnumTurnos TURNO { get; set; }
        public DateTime WHEN { get; set; }
        public string ETLB { get; set; }
        public string DESCRIPCION { get; set; }
        public string WOOP { get; set; }
        public string PERFIL { get; set; }
        public string ACCION { get; set; }
        public string COMENTARIO { get; set; }
        public bool CRITICO { get; set; }
        public bool HECHO { get; set; }
    }
}
