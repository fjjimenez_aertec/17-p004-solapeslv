﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Aertec.SolapesLV.ViewModels;

namespace Aertec.SolapesLV.Controles
{
    /// <summary>
    /// Lógica de interacción para PuntosControlView.xaml
    /// </summary>
    public partial class PuntosControl : UserControl
    {
        PuntosControlViewModel vm;
        public PuntosControl()
        {
            InitializeComponent();
            vm = new PuntosControlViewModel();
            DataContext = vm;
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Height = double.NaN;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Height = 20.0f;
        }

        private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Label lbl = sender as Label;

            StackPanel t = (StackPanel)lbl.Parent;


            TextBox tb = FindVisualChild<TextBox>(t);
            //t.Children;
            //(TextBox)t.Children[1];

            tb.Visibility = Visibility.Visible;
            //DataGridRow row = (DataGridRow)(yourgrid.ItemContainerGenerator.ContainerFromItem(dataGrid.SelectedItem));
            //DataGridDetailsPresenter presenter = FindVisualChild<DataGridDetailsPresenter>(row);
            //DataTemplate template = presenter.ContentTemplate;
            //ComboBox Com = (ComboBox)template.FindName("discountType", presenter);
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
    where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        private void TextBoxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (e.Key == Key.Escape)
            {
                tb.Text = "";
                tb.Visibility = Visibility.Collapsed;
            }
        }
    }
}
