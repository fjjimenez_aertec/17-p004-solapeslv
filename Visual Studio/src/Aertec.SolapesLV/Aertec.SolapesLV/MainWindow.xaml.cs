﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Aertec.SolapesLV.Entities;

namespace Aertec.SolapesLV
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            //createListaSolapes();
            createListaMsn();
            createListaATM();
            createListaOperacional();
            createListaStations();

            OperationalComboBox.DataContext = this;
            ATMComboBox.DataContext = this;
        }

        public List<SolapesEntity> Lista { get; set; }
        public List<MSN> MsnList { get; set; }
        public List<ATM> AtmList { get; set; }
        public List<Operacional> OperacionalList { get; set; }
        public List<String> StationList { get; set; }





        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            var editView = new EditarSolape();
            editView.ShowDialog();
        }


        #region private createMethods
        //private void createListaSolapes()
        //{
        //    Lista = new List<SolapesEntity>();

        //    for (int i = 0; i < 40; i++)
        //    {
        //        Lista.Add(new SolapesEntity()
        //        {
        //            ACCION = "accion" + i,
        //            COMENTARIO = "comentario" + i + "\r\nasdsdsdsad\r\nasdasddsa",
        //            CRITICO = false,
        //            DESCRIPCION = "descripcion" + i,
        //            ETLB = "eTLB" + i,
        //            HECHO = true,
        //            LANZAR = "lanzar" + i,
        //            PERFIL = "perfil" + i,
        //            Prio = "prio" + i,
        //            RESP = "resp" + i,
        //            WOOP = "wo-op" + i,
        //            WHEN = DateTime.Today.AddDays(i)
        //        });
        //    }
        //    datagrid.DataContext = this;
        //}

        private void createListaMsn()
        {
            MsnList = new List<MSN>();

            MsnList.Add(new MSN() { Number = "MSN 77", Country = "spain" });
            MsnList.Add(new MSN() { Number = "MSN 78", Country = "turkey" });

            comboMsn.DataContext = this;
        }

        private void createListaATM()
        {
            AtmList = new List<ATM>();

            AtmList.Add(new ATM() { Name = "Maribel Belizón" });
            AtmList.Add(new ATM() { Name = "Dionisio Lissen" });
            AtmList.Add(new ATM() { Name = "Elena López" });
            AtmList.Add(new ATM() { Name = "Ana Ramírez" });
            AtmList.Add(new ATM() { Name = "Oriol Roca" });
            AtmList.Add(new ATM() { Name = "Fran Moreno" });
            AtmList.Add(new ATM() { Name = "Fernando J. Rendón" });
            AtmList.Add(new ATM() { Name = "David Gámez" });
            AtmList.Add(new ATM() { Name = "Fernando Bolaños" });
            AtmList.Add(new ATM() { Name = "J. Maneul Bermúdez" });
            AtmList.Add(new ATM() { Name = "Raúl Hinojosa" });
            AtmList.Add(new ATM() { Name = "Ricardo Sánchez" });
            AtmList.Add(new ATM() { Name = "María Iglesias" });
            AtmList.Add(new ATM() { Name = "Diego D. Cruzado" });
            AtmList.Add(new ATM() { Name = "Ricardo del Cerro" });
            AtmList.Add(new ATM() { Name = "Borja Perel" });
        }

        private void createListaOperacional()
        {
            OperacionalList = new List<Operacional>();

            OperacionalList.Add(new Operacional() { Name = "Evaristo Tarín" });
            OperacionalList.Add(new Operacional() { Name = "Alejandro Cambra" });
            OperacionalList.Add(new Operacional() { Name = "Antonio Chaparro" });
            OperacionalList.Add(new Operacional() { Name = "Natalia López" });
            OperacionalList.Add(new Operacional() { Name = "Juan Lozano" });
            OperacionalList.Add(new Operacional() { Name = "Itziar Martínez" });
        }

        private void createListaStations()
        {
            StationList = new List<String>();
            StationList.Add("Otras");
            StationList.Add("ST14A");
            StationList.Add("ST14B");
            StationList.Add("DC");
            StationList.Add("MAESE");
            StationList.Add("PINTURA");
            StationList.Add("POS 1");
            StationList.Add("POS 2");
            StationList.Add("POS 3");
            StationList.Add("POS 4B");
            StationList.Add("POS 7B");
            StationList.Add("POS 9");
            StationList.Add("POS 10");
            StationList.Add("POS 13");
            StationList.Add("POS 14");
            StationList.Add("TANGA");
        }
        #endregion

        private void StationComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ComboBox stationComboBox = (ComboBox)sender;
            String station = (String)stationComboBox.SelectedValue;

            if (station == "Otras")
            {
                OtherStation.IsEnabled = true;
            }
            else
            {
                OtherStation.IsEnabled = false;
                OtherStation.Text = "";
            }
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Height = double.NaN;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            tb.Height = 20.0f;
        }

        private void comboMsn_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox msnComboBox = sender as ComboBox;
            MSN xx = (MSN)msnComboBox.SelectedItem;

            Bandera.Source = xx.Image;
        }
    }
}
